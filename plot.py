import random

import matplotlib.pyplot as plt


def subplot(plt, filename, color):
	all_y = []

	with open(filename) as file:
		for line in file:
			y = float(line)
			all_y.append(y)

	plt.plot(all_y, linestyle=':', marker='.', markersize=10, color=color)

	average = sum(all_y) / len(all_y)
	plt.axhline(y=average, color=color, linestyle='-')

def get_title(method):
	if method == 0:
		title = "Trivial"
	elif method == 1:
		title = "Vectorization"
	elif method == 2:
		title = "Parallel"
	elif method == 3:
		title = "Vectorization & parallel"
	else:
		title = "Unknown method"

	return title

def jitter(value):
	jittered_value = value * random.uniform(0.8, 1.2)
	return jittered_value

def plot(input_filename1, input_filename2, output_filename):
	plt.figure(figsize=(16,9))

	subplot(plt, input_filename1, 'red')
	subplot(plt, input_filename2, 'blue')

	plt.ylabel('Iterations / second')
	plt.xlabel('Repetition index')

	plt.savefig(output_filename)


def plot1():
	input_filename = 'results1.txt'
	all_plots = {}

	for method in range(4):
		all_plots[method] = {
			'all_x': [],
			'all_y': [],
		}

	with open(input_filename) as file:
		for line in file:
			values = line.split(',')

			a_nu_rows = int(values[0])
			a_nu_columns = int(values[1])
			b_nu_rows = int(values[2])
			b_nu_columns = int(values[3])
			method = int(values[4])
			iteration = int(values[5])
			nanoseconds = int(values[6])

			nu_elements = a_nu_rows * a_nu_columns * b_nu_rows * b_nu_columns
			plot = all_plots[method]
			# plot['all_x'].append(jitter(nu_elements))
			plot['all_x'].append(nu_elements)
			plot['all_y'].append(nanoseconds)
			all_plots[method] = plot

	for method in all_plots:
		plot = all_plots[method]
		all_x = plot['all_x']
		all_y = plot['all_y']
		output_filename = f'plot1_{method}.png'

		plt.figure(figsize=(16,9))
		plt.scatter(all_x, all_y, marker='x', s=100)
		plt.xscale('log')
		plt.ylabel('Nanoseconds')
		plt.xlabel('Number of elements in the result matrix')

		title = get_title(method)
		plt.title(title)

		plt.savefig(output_filename)


def plot2():
	input_filename = 'results1.txt'
	all_plots = {}

	for method in range(4):
		all_plots[method] = {
			'all_x': [],
			'all_y': [],
			'colors': [],
		}

	with open(input_filename) as file:
		for line in file:
			values = line.split(',')

			a_nu_rows = int(values[0])
			a_nu_columns = int(values[1])
			b_nu_rows = int(values[2])
			b_nu_columns = int(values[3])
			method = int(values[4])
			iteration = int(values[5])
			nanoseconds = int(values[6])
			color = hex(abs(hash(values[0] + values[1] + values[2] + values[3])))[:8].replace('0x', '#')

			nu_elements = a_nu_rows * a_nu_columns * b_nu_rows * b_nu_columns
			plot = all_plots[method]
			plot['all_x'].append(nu_elements)
			plot['all_y'].append(nanoseconds)
			plot['colors'].append(color)
			all_plots[method] = plot

	for method in all_plots:
		plot = all_plots[method]
		all_x = plot['all_x']
		all_y = plot['all_y']
		colors = plot['colors']

		y_of_x = {}
		colors_of_x = {}

		for x, y, color in zip(all_x, all_y, colors):
			if x not in y_of_x:
				y_of_x[x] = []
				colors_of_x[x] = []

			y_of_x[x].append(y)
			colors_of_x[x].append(color)

		plt.figure(figsize=(16,9))

		for x in y_of_x:
			average_y_of_x = sum(y_of_x[x]) / len(y_of_x[x])

			for y, color in zip(y_of_x[x], colors_of_x[x]):

				plt.scatter([jitter(x)], [y/average_y_of_x], marker='x', s=100, c=color)
		
		plt.xscale('log')
		plt.ylabel('Ratio between time and average time of the cluster')
		plt.xlabel('Number of elements in the result matrix')
		
		title = get_title(method)
		plt.title(title)
		
		output_filename = f'plot2_{method}.png'
		plt.savefig(output_filename)

		"""
		average_y_of_x = {}

		for x in y_of_x:
			average_y_of_x[x] = sum(y_of_x[x]) / len(y_of_x[x])


		new_all_x = []
		new_all_y = []

		for x in y_of_x:
			for y in y_of_x[x]:
				ratio = y / average_y_of_x[x]
				new_all_y.append(ratio)
				new_all_x.append(x)

		output_filename = f'plot2_{method}.png'

		plt.figure(figsize=(16,9))
		plt.scatter(new_all_x, new_all_y, marker='x', s=100, c=colors[0])
		plt.xscale('log')
		plt.ylabel('Ratio between time and average time of the cluster')
		plt.xlabel('Number of elements in the result matrix')
		plt.savefig(output_filename)
		"""

def plot3():
	all_methods_x = {}
	all_methods_y = {}

	with open('results2.txt') as file:
		for line in file:
			values = line.split(',')
			n = int(values[0])
			method = int(values[4])
			iteration = int(values[5])
			nanoseconds = int(values[6])

			if method not in all_methods_x:
				all_methods_x[method] = []
				all_methods_y[method] = []

			all_methods_x[method].append(n)
			all_methods_y[method].append(nanoseconds)

	all_methods_averages_x = {}
	all_methods_averages_y = {}

	for method in all_methods_x:
		all_x = all_methods_x[method]
		all_y = all_methods_y[method]
		all_averages_helper = {}

		for x, y in zip(all_x, all_y):
			if x not in all_averages_helper:
				all_averages_helper[x] = []

			all_averages_helper[x].append(y)

		all_methods_averages_x[method] = []
		all_methods_averages_y[method] = []

		for x in all_averages_helper:
			all_methods_averages_x[method].append(x)
			y = sum(all_averages_helper[x]) / len(all_averages_helper[x])
			all_methods_averages_y[method].append(y)

	colors = {
		0: 'red',
		1: 'blue',
		2: 'green',
		3: 'orange',
	}

	names = {
		0: 'Trivial',
		1: 'Vectorization',
		2: 'Parallel',
		3: 'Vectorization & parallel',
	}

	plt.figure(figsize=(16,9))

	for method in all_methods_x:
		all_x = all_methods_x[method]
		all_y = all_methods_y[method]
		color = colors[method]

		plt.scatter(all_x, all_y, marker='x', s=100, c=color)

		averages_x = all_methods_averages_x[method]
		averages_y = all_methods_averages_y[method]

		plt.plot(averages_x, averages_y, marker='', c=color, label=names[method])

	plt.legend()
	plt.minorticks_on()
	plt.yscale('log')
	plt.ylabel('Nanoseconds')
	plt.xlabel('Number of rows/columns of input matrices')
	plt.savefig('plot3_1.png')

	plt.figure(figsize=(16,9))

	for method in all_methods_x:
		all_x = all_methods_x[method]
		all_y = all_methods_y[method]
		new_all_y = [y**0.25 for y in all_y]
		color = colors[method]

		plt.scatter(all_x, new_all_y, marker='x', s=100, c=color)

		averages_x = all_methods_averages_x[method]
		averages_y = all_methods_averages_y[method]
		new_averages_y = [y**0.25 for y in averages_y]

		plt.plot(averages_x, new_averages_y, marker='', c=color, label=names[method])

	plt.legend()
	plt.minorticks_on()
	plt.ylabel('Nanoseconds to the power of 0.25')
	plt.xlabel('Number of rows/columns of input matrices')
	plt.savefig('plot3_2.png')

	plt.figure(figsize=(16,9))

	for method in all_methods_x:
		color = colors[method]
		averages_x = all_methods_averages_x[method]
		averages_y = all_methods_averages_y[method]
		ratios_y = [y / trivial_y for y, trivial_y in zip(averages_y, all_methods_averages_y[0])]
		
		plt.plot(averages_x, ratios_y, marker='x', c=color, label=names[method])

	plt.legend()
	plt.minorticks_on()
	plt.ylabel('Time relative to the trivial algorithm')
	plt.xlabel('Number of rows/columns of input matrices')
	plt.savefig('plot3_3.png')

def main():
	plot1()
	plot2()
	plot3()


if __name__ == '__main__':
	main()
