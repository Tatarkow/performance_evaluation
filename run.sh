#!/bin/bash

for i in $(seq 1 0)
do
	./krocker 1 16 16 16 16 $i
	./krocker 1 256 1 16 16 $i
	./krocker 1 256 16 1 16 $i
	./krocker 1 4096 1 1 16 $i
	./krocker 1 16 1 16 256 $i
	./krocker 1 16 16 1 256 $i
	./krocker 1 16 1 1 4096 $i

	./krocker 1 32 32 32 32 $i
	./krocker 1 1024 1 32 32 $i
	./krocker 1 1024 32 1 32 $i
	./krocker 1 32768 1 1 32 $i
	./krocker 1 32 1 32 1024 $i
	./krocker 1 32 32 1 1024 $i
	./krocker 1 32 1 1 32768 $i

	./krocker 1 64 64 64 64 $i
	./krocker 1 4096 1 64 64 $i
	./krocker 1 4096 64 1 64 $i
	./krocker 1 262144 1 1 64 $i
	./krocker 1 64 1 64 4096 $i
	./krocker 1 64 64 1 4096 $i
	./krocker 1 64 1 1 262144 $i

	./krocker 1 128 128 128 128 $i
	./krocker 1 16384 1 128 128 $i
	./krocker 1 16384 128 1 128 $i
	./krocker 1 2097152 1 1 128 $i
	./krocker 1 128 1 128 16384 $i
	./krocker 1 128 128 1 16384 $i
	./krocker 1 128 1 1 2097152 $i
done

for i in $(seq 0 9)
do
	./krocker 2 16 16 16 16 $i
	./krocker 2 32 32 32 32 $i
	./krocker 2 64 64 64 64 $i
	./krocker 2 128 128 128 128 $i
	./krocker 2 256 256 256 256 $i
done