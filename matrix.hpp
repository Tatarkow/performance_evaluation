#ifndef MATRIX_HPP
#define MATRIX_HPP

#include<stdint.h>
#include<iostream>
#include<fstream>
#include<vector>
#include<stdint.h>
#include<string.h>
#include<thread>

#include "nmmintrin.h" // for SSE
#include "immintrin.h" // for AVX


using data_t = int16_t;


class Matrix {
private:
	void load(std::string filename) {
		std::ifstream file;
	    file.open(filename);

	    if (!file.good()) {
	        std::cout << "ERROR: File '" << filename << "' does not exist." << std::endl;
	        return;
	    }
	    
	   	_nu_rows = 0;
	   	_nu_columns = 0;

	   	file >> _nu_rows;
	   	file >> _nu_columns;

	   	if (_nu_rows <= 0 || _nu_columns <= 0) {
	   		std::cout << "ERROR: Matrix dimensions must be positive integers." << std::endl;
	   		return;
	   	}

	   	uint64_t size = _nu_rows * _nu_columns;
	   	_data.resize(size);

	   	data_t value;
	   	uint64_t nu_values = 0;

	    while (file >> value)
	    {
	        _data[nu_values] = value;
	        nu_values += 1;
	    }

	    if (nu_values != size) {
			std::cout << "ERROR: Matrix dimension and number of values do not match." << std::endl;
	   		return;
	    }
	}

	void kronecker_product_trivial(Matrix& first, Matrix& second) {
		for (uint64_t first_row=0; first_row<first.get_nu_rows(); ++first_row) {
			for (uint64_t first_column=0; first_column<first.get_nu_columns(); ++first_column) {
				for (uint64_t second_row=0; second_row<second.get_nu_rows(); ++second_row) {
					for (uint64_t second_column=0; second_column<second.get_nu_columns(); ++second_column) {
						data_t first_value = first.get(first_row, first_column);
						data_t second_value = second.get(second_row, second_column);
						data_t value = first_value * second_value;

						uint64_t result_row = first_row * second.get_nu_rows() + second_row;
						uint64_t result_column = first_column * second.get_nu_columns() + second_column;
						
						set(value, result_row, result_column);
					}
				}
			}
		}
	}

	void print256(__m256i var) {
	    int16_t val[16];
	    memcpy(val, &var, sizeof(val));

	    printf(
	    	"%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i \n", 
	        val[0], val[1], val[2], val[3], val[4], val[5], val[6], val[7],
	        val[8], val[9], val[10], val[11], val[12], val[13], val[14], val[15]);
	}

	void kronecker_product_vectorization(Matrix& first, Matrix& second) {
		int elements_in_256_bits = 256 / (sizeof(data_t) * 8);

		for (uint64_t first_row=0; first_row<first.get_nu_rows(); ++first_row) {
			for (uint64_t first_column=0; first_column<first.get_nu_columns(); ++first_column) {
				data_t first_scalar = first.get(first_row, first_column);
				const __m256i first_vector = _mm256_set1_epi16(first_scalar);

				for (uint64_t second_row=0; second_row<second.get_nu_rows(); ++second_row) {
					for (uint64_t second_column=0; second_column<second.get_nu_columns(); second_column += elements_in_256_bits) {
						__m256i* second_address = (__m256i*) second.get_address(second_row, second_column);
						const __m256i second_vector = _mm256_loadu_si256(second_address);
						
						uint64_t result_row = first_row * second.get_nu_rows() + second_row;
						uint64_t result_column = first_column * second.get_nu_columns() + second_column;
						__m256i* result_address = (__m256i*) get_address(result_row, result_column);
						__m256i result_vector = _mm256_mullo_epi16(first_vector, second_vector);
						_mm256_storeu_si256(result_address, result_vector);	
					}
				}
			}
		}
	}

	void kronecker_product_parallel(Matrix& first, Matrix& second);

	void kronecker_product_vectorization_and_parallel(Matrix& first, Matrix& second);

	void initialize_threads() {
		// _nu_threads = std::thread::hardware_concurrency();
		_nu_threads = 8;
		_threads.resize(_nu_threads);
	}

	uint64_t _nu_rows;
	uint64_t _nu_columns;
	
	std::vector<data_t> _data;

	int _nu_threads;
	std::vector<std::thread> _threads;


public:
	Matrix(uint64_t nu_rows, uint64_t nu_columns) {
		_nu_rows = nu_rows;
		_nu_columns = nu_columns;

		uint64_t size = _nu_rows * _nu_columns;
	   	_data.resize(size);

	   	for (uint64_t i=0; i<size; ++i) {
	   		_data[i] = 0;
	   	}

	   	initialize_threads();
	}

	Matrix(std::string filename) {
	    load(filename);
	    initialize_threads();
	}

	uint64_t get_nu_rows() {
		return _nu_rows;
	}

	uint64_t get_nu_columns() {
		return _nu_columns;
	}

	data_t* get_address(uint64_t row, uint64_t column) {
		uint64_t index = row * _nu_columns + column;
		return &_data[index];
	}

	void set(data_t value, uint64_t row, uint64_t column) {
		uint64_t index = row*_nu_columns + column;
		_data[index] = value;
	}

	data_t get(uint64_t row, uint64_t column) {
		uint64_t index = row*_nu_columns + column;
		return _data[index];
	}

	void generate_random() {
		uint64_t size = _nu_rows * _nu_columns;

		for (uint64_t i=0; i<size; ++i) {
			int sign = (rand() % 2) * 2 - 1;
			_data[i] = sign * (rand() % (1<<8));
		}
	}


	void kronecker_product(Matrix& first, Matrix& second, int method) {
		if (first.get_nu_rows() * second.get_nu_rows() != _nu_rows) {
			std::cout << "ERROR: Number of rows do not match." << std::endl;
	   		return;
		}

		if (first.get_nu_columns() * second.get_nu_columns() != _nu_columns) {
			std::cout << "ERROR: Number of columns do not match." << std::endl;
	   		return;
		}

		if (method == 0) {
			kronecker_product_trivial(first, second);
		}
		else if (method == 1) {
			kronecker_product_vectorization(first, second);
		}
		else if (method == 2) {
			kronecker_product_parallel(first, second);
		}
		else if (method == 3) {
			kronecker_product_vectorization_and_parallel(first, second);
		}
		else {
			std::cout << "WARNING: Unknown method. Trivial will be used." << std::endl;
			kronecker_product_trivial(first, second);
		}
		
	}

	void print() {
		for (int i=0; i<80; ++i) {
			std::cout << "-";
		}

		std::cout << std::endl;

		for (uint64_t row=0; row<_nu_rows; ++row) {
			for (uint64_t column=0; column<_nu_columns; ++column) {
				uint64_t index = row*_nu_columns + column;
				std::cout << _data[index] << "\t";
			}

			std::cout << std::endl;
		}

		for (int i=0; i<80; ++i) {
			std::cout << "-";
		}

		std::cout << std::endl << std::endl;
	}

	void print(std::string filename) {
		std::ofstream file;
		file.open(filename);
		
		for (uint64_t row=0; row<_nu_rows; ++row) {
			for (uint64_t column=0; column<_nu_columns; ++column) {
				uint64_t index = row*_nu_columns + column;
				file << _data[index] << "\t";
			}

			file << std::endl;
		}

		file.close();
	}
};


void kronecker_product_parallel_thread(uint64_t first_begin, uint64_t first_end, Matrix* first, Matrix* second, Matrix* result) {
	for (uint64_t first_row=first_begin; first_row<first_end; ++first_row) {
		for (uint64_t first_column=0; first_column<first->get_nu_columns(); ++first_column) {
			for (uint64_t second_row=0; second_row<second->get_nu_rows(); ++second_row) {
				for (uint64_t second_column=0; second_column<second->get_nu_columns(); ++second_column) {
					data_t first_value = first->get(first_row, first_column);
					data_t second_value = second->get(second_row, second_column);
					data_t value = first_value * second_value;

					uint64_t result_row = first_row * second->get_nu_rows() + second_row;
					uint64_t result_column = first_column * second->get_nu_columns() + second_column;
					
					result->set(value, result_row, result_column);
				}
			}
		}
	}
}


void kronecker_product_vectorization_and_parallel_thread(uint64_t first_begin, uint64_t first_end, Matrix* first, Matrix* second, Matrix* result) {
	int elements_in_256_bits = 256 / (sizeof(data_t) * 8);

	for (uint64_t first_row=first_begin; first_row<first_end; ++first_row) {
		for (uint64_t first_column=0; first_column<first->get_nu_columns(); ++first_column) {
			data_t first_scalar = first->get(first_row, first_column);
			const __m256i first_vector = _mm256_set1_epi16(first_scalar);

			for (uint64_t second_row=0; second_row<second->get_nu_rows(); ++second_row) {
				for (uint64_t second_column=0; second_column<second->get_nu_columns(); second_column += elements_in_256_bits) {
					__m256i* second_address = (__m256i*) second->get_address(second_row, second_column);
					const __m256i second_vector = _mm256_loadu_si256(second_address);
					
					uint64_t result_row = first_row * second->get_nu_rows() + second_row;
					uint64_t result_column = first_column * second->get_nu_columns() + second_column;
					__m256i* result_address = (__m256i*) result->get_address(result_row, result_column);
					__m256i result_vector = _mm256_mullo_epi16(first_vector, second_vector);
					_mm256_storeu_si256(result_address, result_vector);	
				}
			}
		}
	}
}


void Matrix::kronecker_product_parallel(Matrix& first, Matrix& second) {
	uint64_t nu_rows_in_batch = first.get_nu_rows() / _nu_threads;

	for (int i=0; i<_nu_threads; ++i) {
		uint64_t first_begin = i*nu_rows_in_batch;
		uint64_t first_end = (i+1)*nu_rows_in_batch;
		_threads[i] = std::thread(kronecker_product_parallel_thread, first_begin, first_end, &first, &second, this);
	}

	for (int i=0; i<_nu_threads; ++i) {
        _threads[i].join();
    }
}


void Matrix::kronecker_product_vectorization_and_parallel(Matrix& first, Matrix& second) {
	uint64_t nu_rows_in_batch = first.get_nu_rows() / _nu_threads;

	for (int i=0; i<_nu_threads; ++i) {
		uint64_t first_begin = i*nu_rows_in_batch;
		uint64_t first_end = (i+1)*nu_rows_in_batch;
		_threads[i] = std::thread(kronecker_product_vectorization_and_parallel_thread, first_begin, first_end, &first, &second, this);
	}

	for (int i=0; i<_nu_threads; ++i) {
        _threads[i].join();
    }
}


#endif
