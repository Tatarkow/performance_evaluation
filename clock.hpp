#include <chrono>


class Clock {
private:
	std::chrono::steady_clock::time_point _begin;
	

public:
	void start() {
		_begin = std::chrono::steady_clock::now();
	}

	long stop() {
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		return std::chrono::duration_cast<std::chrono::nanoseconds> (end - _begin).count();
	}
};