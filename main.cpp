#include<iostream>
#include <string> 

#include "matrix.hpp"
#include "clock.hpp"

using namespace std;


void test0() {
	Clock clock;
	srand(0);

	uint64_t a_nu_rows = 1<<5;
	uint64_t a_nu_columns = 1<<5;
	Matrix a(a_nu_rows, a_nu_columns);
	a.generate_random();
	a.print("a.out");

	uint64_t b_nu_rows = 1<<5;
	uint64_t b_nu_columns = 1<<5;
	Matrix  b(b_nu_rows, b_nu_columns);
	b.generate_random();
	b.print("b.out");
	
	for (int method=0; method<=3; ++method) {
		uint64_t c_nu_rows = a_nu_rows * b_nu_rows;
		uint64_t c_nu_columns = a_nu_columns * b_nu_columns;
		Matrix c(c_nu_rows, c_nu_columns);

		clock.start();
		c.kronecker_product(a, b, method);
		clock.stop();
		
		c.print("c" + std::to_string(method) + ".out");
	}
}


void test1(char** argv) {
	uint64_t a_nu_rows = stoi(argv[2]);
	uint64_t a_nu_columns = stoi(argv[3]);
	uint64_t b_nu_rows  = stoi(argv[4]);
	uint64_t b_nu_columns  = stoi(argv[5]);
	int iteration = stoi(argv[6]);

	Clock clock;
	srand(0);

	Matrix a(a_nu_rows, a_nu_columns);
	a.generate_random();

	Matrix  b(b_nu_rows, b_nu_columns);
	b.generate_random();
	
	for (int method=0; method<=3; ++method) {
		uint64_t c_nu_rows = a_nu_rows * b_nu_rows;
		uint64_t c_nu_columns = a_nu_columns * b_nu_columns;
		Matrix c(c_nu_rows, c_nu_columns);

		clock.start();
		c.kronecker_product(a, b, method);
		long nanoseconds = clock.stop();

		std::cout << a_nu_rows << "," << a_nu_columns << ",";
		std::cout << b_nu_rows << "," << b_nu_columns << ",";
		std::cout << method << "," << iteration << "," << nanoseconds << std::endl;
	}
}


int main(int argc, char** argv) {
	if (stoi(argv[1]) == 0) {
		test0();
	}
	else if (stoi(argv[1]) == 1) {
		test1(argv);
	}
	else if (stoi(argv[1]) == 2) {
		test1(argv);
	}
	
	return 0;
}